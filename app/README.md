#### The NHGRI-EBI GWAS Catalog

The [NHGRI-EBI GWAS Catalog](https://www.ebi.ac.uk/gwas/) ([MacArthur et al. 2017](https://academic.oup.com/nar/article/45/D1/D896/2605751), [Buniello et al. 2019](https://academic.oup.com/nar/article/47/D1/D1005/5184712)) is a curated database with tens of thousands of SNPs found in GWAS studies performed in human populations for multiple quantitative traits and diseases.

As it contains information regarding mapped loci effect sizes and frequencies, it allows a better understanding of the architecture of complex traits in humans and the distribution of locus effect sizes.

----------------------------------------------------------------------------

#### Locus effect sizes and contributions to heritability

GWEHS allows a subset of traits from the GWAS catalog to be selected, for which SNP with both effect and frequency recorded will be read. From that set of SNPs, only one SNP per locus mapped will be considered (the most recent one, and then the most significative). The user can also filter the input SNPs by selecting a P-value threshold or a value of genomic distance (in Mb) between SNPs. SNPs with extremely high (or low) effect sizes can also be removed as outliers. Note that when selecting more than one trait, these will be analysed together.

Values of effect size (BETA or *β*), frequency (*q*) and contribution to heritability (*h²*) for each locus is given in a downloadable table. Heritability is estimated following the classical formula (Falconer and Mackay, 1996), assuming that average effects of a gene substitution (*α*) are in standardized units (e.g. beta-coefficient), i.e. *β = α/ sd(P)*.

*h² = 2β² q (1-q)*

If the trait has its effect measured as odd ratio (OR), a value of prevalence must be given. Note that the effect type of the trait is always indicated in the box text together with the distribution of effect sizes. Prevalence is ignored for other traits.

**Distribution of effect sizes:** locus effect sizes are fitted for different parametric distributions, and the more likely one will be shown by default, though these can also be fitted for different distributions. Data will be automatically rescaled to fit within the range (0,1) when using beta and logistic distributions, and when negative effects are detected.

**Distribution of effect sizes with MAF:** locus individual effect sizes can also be shown in a scatterplot by their MAFs value. This option allows individual loci (e.g. with extreme values) to be identified.

**Locus contributions to heritability:** Loci are classified by their effect size into three groups (low, medium and high effect sizes), and their contribution to heritability is measured. The percentage of loci belonging to each of these effect size types can be modified (from 0 to 50% for low an high effect sizes).

----------------------------------------------------------------------------

#### Inferring the missing heritability

This application uses methods from [López-Cortegano and Caballero (2019b)](https://doi.org/10.1534/genetics.119.302077) in order to infer the change in the mean effect size and heritability as new loci are being discovered in more recent publications. The regression method by default is a two-parameter exponential model (y=a·x^b), but it can be changed to linear (y=a+x·b) or four-parameter logistic regression (y=c+(d-c)/(1+exp(b·(ln(x) - ln(e))))) at will. Predictions will be made considering a candidate number of additional loci to be found (2,000 by default), up to 10,000.

**Change in mean effect size:** the mean locus effect size is measured as new loci are being discovered in articles sorted by their publication date. Regression is used to fit that change.

**Change in heritability:** the heritability explained by loci as these are being found in more recent publications is fitted to a regression model.

**Missing heritability:** regression is used to fit the change in the parameters of the distribution of loci effect sizes and MAF. Then, from the expected distributions, new loci are sampled and their contribution to heritability is computed.

----------------------------------------------------------------------------

#### Assistance

The source code is fully available at [GitLab](https://gitlab.com/elcortegano/GWEHS).  There you can find an [issue tracker](https://gitlab.com/elcortegano/GWEHS/issues) where to submit any suggestions and to request additional features.

E-mail application authors:

- [Eugenio López-Cortegano](mailto:e.lopez@uvigo.es)  

- [Armando Caballero](mailto:armando@uvigo.es)  

----------------------------------------------------------------------------

#### Citation

If you use GWEHS in your research, please cite:

 * López-Cortegano, E. and Caballero, A. (2019a). *GWEHS: A Genome-Wide Effect sizes and Heritability Screener*. Genes 10(8): 558.

----------------------------------------------------------------------------

#### References

- Buniello, A. et al. (2019). *The NHGRI-EBI GWAS Catalog of published genome-wide association studies, targeted arrays and summary statistics 2019*. Nucleic Acids Res. 47 (D1): D1005–D1012.

- Falconer, DS., and Mackay, TFC. (1996). *Introduction to Quantitative Genetics*. 4th ed. Harlow, Essex, UK: Longmans Green.

- López-Cortegano, E. and Caballero, A. (2019b). *Inferring the nature of missing heritability in human traits using data from the GWAS catalog*. Genetics 212(3): 891-904.

- MacArthur, J. et al. (2017). *The new NHGRI-EBI Catalog of published genome-wide association studies (GWAS Catalog)*. Nucleic Acids Res. 45 (D1): D896–D901.

- So, H. et al. (2011). *Evaluating the heritability explained by known susceptibility variants: A survey of ten complex diseases*. Genet. Epidemiol. 35 (5): 310–317.
