# Install R version 4.0.2
FROM r-base:4.3.1

# Install Ubuntu packages
RUN apt-get update && apt-get upgrade -y && apt-get install -y \
    sudo \
    gdebi-core \
    cmake \
    libcurl4-gnutls-dev \
    libxml2-dev \
    libssl-dev \
    libgmp3-dev \
    libffi-dev

# Download and install ShinyServer (latest version)
RUN wget --no-verbose https://s3.amazonaws.com/rstudio-shiny-server-os-build/ubuntu-12.04/x86_64/VERSION -O "version.txt" && \
    VERSION=$(cat version.txt)  && \
    wget --no-verbose "https://s3.amazonaws.com/rstudio-shiny-server-os-build/ubuntu-12.04/x86_64/shiny-server-$VERSION-amd64.deb" -O ss-latest.deb && \
    gdebi -n ss-latest.deb && \
    rm -f version.txt ss-latest.deb && \
    chown shiny:shiny /var/lib/shiny-server

# Install R packages that are required
RUN sudo su - -c "R -e \"install.packages(c('tidyverse', 'shiny', 'flexdashboard','rmarkdown', 'stringr', 'dplyr', 'ggplot2', 'plotly', 'DT', 'drc', 'fitdistrplus'), repos='http://cran.rstudio.com/')\""

# Copy configuration files into the Docker image
COPY shiny-server.conf /etc/shiny-server/shiny-server.conf
COPY . /srv/shiny-server/
WORKDIR /srv/shiny-server

# Make the ShinyApp available at port 80
EXPOSE 80

CMD ["/usr/bin/shiny-server"]
