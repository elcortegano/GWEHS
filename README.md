# GWEHS: Genome-Wide Effect sizes and Heritability Screener

Currently hosted at:

- [http://gwehs.uvigo.es](http://gwehs.uvigo.es)

Note that the app can also be run app locally (see 'Run as server' section below).

## Overview

GWEHS is an open-source application to screen loci associated to human complex traits and diseases from the [NHGRI-EBI GWAS Catalog](https://www.ebi.ac.uk/gwas/) ([MacArthur et al. 2017](https://academic.oup.com/nar/article/45/D1/D896/2605751), [Buniello et al. 2019](https://academic.oup.com/nar/article/47/D1/D1005/5184712)) database. This application provides a way to explore the distribution of effect sizes of loci affecting these traits, as well as their contribution to heritability. Furthermore, it allows for making predictions on the change in the expected mean effect size as well as in the heritability as new loci are found. The application enables inferences on whether the additive contribution of loci expected to be discovered in the future will be able to explain the estimates of familial heritability for the different traits. 

This tool uses methods from a recent meta-analyses by [López-Cortegano and Caballero 2019](https://doi.org/10.1534/genetics.119.302077), where records from the GWAS Catalog were used in order to investigate the nature of missing heritability in humans. Results from that investigation indicate that the distribution of variant effects fits well to a log-normal distribution and that the average variant effect size decreases exponentially as new variants are found, contributing little to heritability. The predictions on the expected distribution of variant effects for many traits showed that the additive effects of variants yet to be discovered cannot explain alone the missing heritability, suggesting that other sources of variation are involved.

## Usage

The application has two main sections or pages. Documentation can be found on the About section, as well as in the application README file of this repository (see [app/README.md](app/README.md)).

Analyses can be performed in the Explore section of the application. In short, different traits and options can be selected in the left sidebar, and results will be interactively generated on the central boxes in the form of figures and tables. In these, the blue colour will indicate oberved values, while fitted values and predictions will be shown in red.

These results can also be grouped in two sets. The first row corresponds to exploratory analyses on the distribution on locus effect sizes, and their contribution to heritability. Loci used for these analyses are shown in a downloadable table.

In a second row, inferences on the change of the mean effect size and heritability are shown. In the case of heritability, these are performed by regression on the heritability explained as new loci are found, and also from inferences on the expected distribution of locus effect sizes (see [López-Cortegano and Caballero 2019](https://doi.org/10.1534/genetics.119.302077)).

## Run as server

GWEHS can be run as an online application, but it can also be run on a local server, with the main advantage of it being independent of the network status, and probably showing a higher performance. Developers can set up this server in three different ways:

#### Using a Docker image

[Docker](https://www.docker.com/) provides a way to use an image of the application in a cross-platforms way. This will include the web server itself and all required configuration files. Thus, GWEHS can be served locally just by downloading its docker image:

    docker pull elcortegano/gwehs:latest

And running it:

    docker run -p 80:80 elcortegano/gwehs:latest

Then, the server can be accessed simply opening a web browser and entering `localhost` as web address.

Note that Docker must be installed to use this option, and that Windows users may need to run a 64-bits system with Windows 10 and virtualization enabled (this can be done in the UEFI/BIOS settings).

#### Using R Studio

[R Studio](https://www.rstudio.com/) is a popular interface for programming in R and running R scripts, including R markdown files and interactive [Shiny](https://shiny.rstudio.com/) applications, as GWEHS.

GWEHS can be rendered by a built-in function in R Studio. Just install all required packages by the [gwehs.Rmd](app/gwehs.Rmd) file, open it and press the *Run document* button to create a local instance of the application. Note that this action requires to have a processed GWAS Catalog database on advance. To do so, on the root directory of the application just run the [preprocessing.sh](preprocessing.sh) script file:

    bash preprocessing.sh

It may take a while (up to 4-5 hours).

#### Configuring a Shiny server

More advanced users can set up their own shiny server, instructions to do so can be found in the [Shiny server](https://github.com/rstudio/shiny-server) page at GitHub. These can be useful for users interested in running their own shiny applications and make them available on the web.

A full explanation of how to configure this server and its options is out of the scope of this application, but note that all required steps to do so are run from within the [Dockerfile](Dockerfile) distributed with this package, together with a [shiny server configuration file](shiny-server.conf). 

## Assistance

Please use the [issue tracker](https://gitlab.com/elcortegano/GWEHS/issues) to submit any suggestions and to request additional features.

## Citation

If you use GWEHS in your research, please cite:

 * López-Cortegano, E. and Caballero, A. (2019). *GWEHS: A Genome-Wide Effect sizes and Heritability Screener*. Genes 10(8): 558.

## References

- Buniello, A. et al. (2019). *The NHGRI-EBI GWAS Catalog of published genome-wide association studies, targeted arrays and summary statistics 2019*. Nucleic Acids Res. 47 (D1): D1005–D1012.

- López-Cortegano, E. and Caballero, A. (2019). *Inferring the nature of missing heritability in human traits using data from the GWAS catalog*. Genetics (accepted).

- MacArthur, J. et al. (2017). *The new NHGRI-EBI Catalog of published genome-wide association studies (GWAS Catalog)*. Nucleic Acids Res. 45 (D1): D896–D901.
