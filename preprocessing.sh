#!/bin/sh

GW="gwascatalog.tsv"
GV="gversion.txt"
GD="gdate.txt"
CS="checksum.txt"
APP="app/"
DEST="${APP}db/"

mkdir -p ${DEST}
rm -f *.tsv
rm -f ${APP}${GV}
wget --content-disposition https://www.ebi.ac.uk/gwas/api/search/downloads/alternative
if [ $? != 0 ]; then echo "Failed to download the GWAS Catalog. Are you connected to the Internet?"; fi
ls *.tsv | tr '_' ' ' | cut -d ' ' -f4,5 | cut -d '.' -f1 > ${APP}${GV}
date '+%d %B %Y' > ${APP}${GD}
mv -f *.tsv ${DEST}${GW}

md5sum ${DEST}${GW} > ${DEST}test_${CS}

mv -f ${DEST}test_${CS} ${DEST}${CS}
cp preprocessing*.R ${DEST}
cd ${DEST}
Rscript --vanilla preprocessing.R
rm preprocessing*.R
